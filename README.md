# Setuptools C++ CUDA

***Warning:*** This project has been renamed to [pypi/setuptools-cuda-cpp](https://pypi.org/project/setuptools-cuda-cpp)
for visibility reasons in Pypi and migrated to GitHub for enhance community Issues feedback.

Please uninstall this module:

```pip uninstall setuptools-cpp-cuda```

and install the new package:

```pip install setuptools-cuda-cpp```

Sorry for the inconveniences.